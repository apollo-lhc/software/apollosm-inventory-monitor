#!/usr/bin/env python3

import os
import sys
import time
import yaml
import signal
import argparse
import logging
import requests

# Daemon related imports
import pidfile
import daemon
import sdnotify
import signal

from threading import Event
from enum import IntEnum
from util.apollo import Apollo
from util.register_helper import RegisterHelper

from helpers.db import (
    retrieve_response_from_snipeit_db,
    get_apollosm_item,
    find_assigned_item_to_service_module,
    get_custom_field_from_db_item
    )
from helpers.git_utils import (
    compare_git_version_with_latest_tag,
    FWStatus
)

from ApolloSM import ApolloSM

# If the internal flag of this Event object is set to true
# (by SIGTERM handler), the application stops gracefully.
EXIT_EVENT = Event()

PIDFILE_PATH = '/var/run/apollosm-inventory-monitor.pid'             # PID File Path (Configured in service file)
#LOGFILE_PATH = '/var/log/apollosm-inventory-monitor.log'
#CONFIG_FILE_PATH = '/etc/apollosm-inventory-monitor'                 # YAML configuration file for this daemon
LOGFILE_PATH = 'debugging.log' # For running nodaemon
CONFIG_FILE_PATH = 'config.yaml' # For running nodaemon

# Set up logging for this daemon
LOGGER = logging.getLogger()

formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%d-%b-%y %H:%M:%S')
file_handler = logging.FileHandler(LOGFILE_PATH, 'a')
file_handler.setFormatter(formatter)

LOGGER.addHandler(file_handler)
LOGGER.setLevel(logging.INFO)

# Default values to assign if there is no device found
DEFAULT_FW_VALUE = '-999'
DEFAULT_UID_VALUE = '-999'
DEFAULT_FS_HASH = '-999'

class DBMatchStatus(IntEnum):
    """
    Enum representing the state of DB match.
    """
    DB_ERROR = 0    # Error connecting to the DB
    NO = 1          # Installed fw does NOT match the DB entry
    YES = 2         # Installed fw matches the DB entry 

def parse_cli():
    """Command line parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--nodaemon", help="Don't daemonize this process", action="store_true")
    args = parser.parse_args()
    return args

def handle_db_error(reghelper):
    """
    Callback function in case DB connection fails.
    """
    for register in ["sm_fw_sha.is_latest_tag", "cm_fw.is_latest_tag", "ipmc_fw.is_latest_tag", "zynq_fs.is_latest_tag"]:
        reghelper.write(register, FWStatus.DB_ERROR.value)
    
    for register in ["sm_fw_sha.db_match", "sm_fw_version.db_match", "cm_fw.db_match", "ipmc_fw.db_match", "ipmc_uid.db_match", "zynq_uid.db_match"]:
        reghelper.write(register, DBMatchStatus.DB_ERROR.value)

def get_api_keys(config: dict) -> dict:
    """
    Return all API keys as a dictionary.
    Throws an AssertionErrror if all API keys are not there.
    """
    assert "api_tokens" in config, "'api_tokens' field not found in the config."
    tokens = config["api_tokens"]

    token_types = ["snipeit", "github", "gitlab"]
    for token_type in token_types:
        assert token_type in tokens, f"{token_type} token not found in the config."

    return tokens


def func_daemon(config: dict, sm_instance: ApolloSM):

    def signal_handler(signum, frame):
        """
        Signal handler to deal with SIGTERM signal coming from docker stop.
        """
        LOGGER.info(f'Interrupted by signal {signum}, shutting down.')

        # Sets the internal flag of EXIT_EVENT to True, so that
        # we stop at the next iteration
        EXIT_EVENT.set()

    # Register the SIGINT signal to the custom signal_handler function
    signal.signal(signal.SIGINT, signal_handler)

    # Inform systemd that we've finished our startup sequence
    # This allows systemd to start other services that depend
    # on this one.
    notifier = sdnotify.SystemdNotifier()
    notifier.notify("READY=1")

    # 
    # Run the monitoring application as long as we keep running
    # 
    while not EXIT_EVENT.is_set():
        run_app(config, sm_instance)
        
        LOGGER.info(f"Sleeping for {config['poll_time']} seconds.")
        EXIT_EVENT.wait(config["poll_time"])
    
    # SIGINT signal received, shut down
    sys.exit(0)

def read_serial_from_mac_file(file_path: str) -> int:
    """
    Reads the serial number from the MAC address file. The serial number is determined by
    extracting the last section of the MAC address and converting it to an integer.

    Args:
        file_path (str): Path to the MAC address file.

    Returns:
        int: The serial number extracted from the MAC address.
    """
    try:
        # Read the content of the file
        with open(file_path, "r") as f:
            mac_address = f.read().strip()
        
        # Split the MAC address by colons
        mac_parts = mac_address.split(":")
        
        # Extract the last part of the MAC address and convert it to an integer (base 16)
        serial = int(mac_parts[-1], 16)
        return serial

    except FileNotFoundError:
        LOGGER.error(f"File not found: {file_path}")
        return None
    except ValueError as e:
        LOGGER.error(f"Failed to convert MAC address to serial number: {e}")
        return None
    except Exception as e:
        LOGGER.error(f"Unexpected error while reading serial number: {e}")
        return None

    
def run_app(config: dict, sm_instance: ApolloSM):
    """
    Function to run the monitoring application for a single iteration. 
    """
    LOGGER.info(f'Execution started: {time.ctime()}')

    # RegisterHelper to help with register reads and writes
    reghelper = RegisterHelper(config["registers"], sm_instance, logger=LOGGER)

    # Obtain serial number from MAC file in /fw/SM
    serial = read_serial_from_mac_file("/fw/SM/eth1_mac.dat")
    print(serial)
    # Construct Apollo object with proper serial number
    apollo = Apollo(serial)

    # Read the SM FW hash from Zynq
    try:
        sm_fw_hash = reghelper.read("sm.fw")
        
    # Handle the case where we can't read the FW hash
    except RuntimeError as e:
        LOGGER.warning(str(e))
        sm_fw_hash = DEFAULT_FW_VALUE

    # Read the SM Version number form Zynq
    try:
        sm_fw_ver = reghelper.read("sm.version")

    # Handle the case where we can't read the FW hash
    except RuntimeError as e:
        LOGGER.warning(str(e))
        sm_fw_ver = DEFAULT_FW_VALUE
        
    # Read the Zynq FS Hash
    try:
        if not os.path.isfile(config['zynq_fs']):
            raise RuntimeError("Zynq_fs path in config is not a file")
        with open(config['zynq_fs'], 'r') as f:
            lines = f.read().splitlines()
            if not lines:
                raise RuntimeError("/etc/fs_version is empty")
            
            last_line = lines[-1]
            zynq_fs_hash = last_line[-14:]
    except Exception as e:
        print(f"An error occurred: {e}")
    
    except RuntimeError as e:
        LOGGER.warning(str(e))
        zynq_fs_hash = DEFAULT_FS_HASH 

    # Read the ZYNQ DNA Words, convert to string
    try:
        if reghelper.read("zynq.valid") == 0:
            raise RuntimeError("Zynq DNA is invalid")
        total_word = []
        for i in range(3):
            current_word = reghelper.read("zynq.dna_" + str(i))
            total_word.append(current_word.replace('0x', '').rjust(8, '0').upper())
        zynq_UID_hash = ''.join(total_word)
    except RuntimeError as e:
        LOGGER.warning(str(e))
        zynq_UID_hash = DEFAULT_UID_VALUE

    # Read the FW hashes for CM from Zynq register  
    try:
        cm_fw_hash = reghelper.read("cm.fw")
    
    # Handle the case where we can't read the FW hash
    except RuntimeError as e:
        LOGGER.warning(str(e))
        cm_fw_hash = DEFAULT_FW_VALUE 

    # Read the FW hashes for IPMC from Zynq register  
    try:
        ipmc_fw_hash = reghelper.read("ipmc.fw")
    
    # Handle the case where we can't read the FW hash
    except RuntimeError as e:
        LOGGER.warning(str(e))     
        ipmc_fw_hash = DEFAULT_FW_VALUE

    try:
        ipmc_UID_hash = reghelper.read("ipmc.UID")
    
    # Handle the case where we can't read the FW hash
    except RuntimeError as e:
        LOGGER.warning(str(e))     
        ipmc_UID_hash = DEFAULT_UID_VALUE

    apollo.set_sm_firmware_version("fromZynq", sm_fw_hash)
    apollo.set_sm_firmware_extra_version("fromZynq", sm_fw_ver)
    apollo.set_cm_firmware_version("fromZynq", cm_fw_hash)
    apollo.set_ipmc_firmware_version("fromZynq", ipmc_fw_hash)
    apollo.set_ipmc_uniqueID("fromZynq", ipmc_UID_hash)
    apollo.set_zynq_filesystem_version("fromETC", zynq_fs_hash)
    apollo.set_zynq_uniqueID("fromZynq", zynq_UID_hash)
    
    # 
    # Retrieve FW information from SnipeIT DB
    # 

    # Retrieve tokens
    try:
        api_tokens = get_api_keys(config)
    except AssertionError as e:
        LOGGER.error(str(e))
        return

    # Retrieve HW items from SnipeIT DB
    try:
        hw_items = retrieve_response_from_snipeit_db(
            config["snipeit"]["base_url"], 
            api_tokens["snipeit"]
        )
    # Handle connection failure
    except requests.exceptions.ConnectionError:
        LOGGER.error("Could not connect to the SnipeIT database, exiting.")
        # Write DB error and exit
        handle_db_error(reghelper)
        return

    # Check if the response is valid
    if not hw_items:
        LOGGER.error('Could not retrieve a valid response from SnipeIT, exiting.')
        # Write DB error and exit
        handle_db_error(reghelper)
        return
    
    # Get the ApolloSM item with the correct serial from the DB
    apollosm_db = get_apollosm_item(hw_items, serial=serial)
    if not apollosm_db:
        LOGGER.error(f"Could not locate ApolloSM in DB with serial: {serial}")
        return 

    # Retrieve SM FW hash from DB and set
    sm_fw_hash_db = get_custom_field_from_db_item(apollosm_db, "ApolloSM Global SHA")
    apollo.set_sm_firmware_version("fromDB", sm_fw_hash_db)

    # Retrieve SM FW Version from DB and set
    sm_fw_ver_db = get_custom_field_from_db_item(apollosm_db, "ApolloSM Global Version")
    apollo.set_sm_firmware_extra_version("fromDB", sm_fw_ver_db)

    # Retrieve Zynq FS hash from DB and set
    zynq_fs_hash_db = get_custom_field_from_db_item(apollosm_db, "Zynq Filesystem Version")
    apollo.set_zynq_filesystem_version("fromDB", zynq_fs_hash_db)

    # Retrive Zynq UID hash from DB and set
    zynq_uniqueID_db = get_custom_field_from_db_item(apollosm_db, "Zynq Unique ID")
    apollo.set_zynq_uniqueID("fromDB", zynq_uniqueID_db)

    # Retrieve the assigned CM and IPMC from the DB
    apollocm_db   = find_assigned_item_to_service_module(hw_items, apollosm_db["id"], "Apollo Blade Command Module")
    apolloipmc_db = find_assigned_item_to_service_module(hw_items, apollosm_db["id"], "IPMC")

    # If a CM is found, set it's FW version
    if apollocm_db:
        cm_fw_hash_db = get_custom_field_from_db_item(apollocm_db, "ApolloCM FW Version")
        apollo.set_cm_firmware_version("fromDB", cm_fw_hash_db)

    # If there is no CM, set the FW hash to be 0x0, because that will be
    # the default register value read from Zynq
    else:
        apollo.set_cm_firmware_version("fromDB", DEFAULT_FW_VALUE)

    # If a IPMC is found, set it's FW version and uniqueID
    if apolloipmc_db:
        ipmc_fw_hash_db  = get_custom_field_from_db_item(apolloipmc_db, "IPMC FW Commit Hash")
        ipmc_uniqueID_db = get_custom_field_from_db_item(apolloipmc_db, "IPMC Unique ID")
        apollo.set_ipmc_firmware_version("fromDB", ipmc_fw_hash_db)
        apollo.set_ipmc_uniqueID("fromDB", ipmc_uniqueID_db)

    # If there is no IPMC, set the FW hash to be 0x0, because that will be
    # the default register value read from Zynq
    else:
        apollo.set_ipmc_firmware_version("fromDB", DEFAULT_FW_VALUE)
        apollo.set_ipmc_uniqueID("fromDB", DEFAULT_UID_VALUE)

    # Print the FW versions out
    apollo.list_firmware_versions(logger=LOGGER)

    # Write to a register indicating if the FW versions are compatible or not
    sm_fw_hash_db_match = DBMatchStatus.YES if apollo.sm_fw_versions_match() else DBMatchStatus.NO
    sm_fw_version_db_match = DBMatchStatus.YES if apollo.sm_fw_extra_versions_match() else DBMatchStatus.NO
    cm_fw_db_match = DBMatchStatus.YES if apollo.cm_fw_versions_match() else DBMatchStatus.NO
    ipmc_fw_db_match = DBMatchStatus.YES if apollo.ipmc_fw_versions_match() else DBMatchStatus.NO
    ipmc_uniqueID_db_match = DBMatchStatus.YES if apollo.ipmc_uniqueID_match() else DBMatchStatus.NO
    zynq_fs_db_match = DBMatchStatus.YES if apollo.zynq_filesystem_versions_match() else DBMatchStatus.NO
    zynq_uniqueID_db_match = DBMatchStatus.YES if apollo.zynq_uniqueID_match() else DBMatchStatus.NO

    reghelper.write("sm_fw_sha.db_match", sm_fw_hash_db_match.value)
    reghelper.write("sm_fw_version.db_match", sm_fw_version_db_match.value)
    reghelper.write("cm_fw.db_match", cm_fw_db_match.value)
    reghelper.write("ipmc_fw.db_match", ipmc_fw_db_match.value)
    reghelper.write("ipmc_uid.db_match", ipmc_uniqueID_db_match.value)
    reghelper.write("zynq_fs.db_match", zynq_fs_db_match.value)
    reghelper.write("zynq_uid.db_match", zynq_uniqueID_db_match.value)
    # 
    # GitHub & GitLab API: 
    # Get the latest FW tags from GitHub/GitLab and compare with the installed FW versions.
    # 
    compare_git_version_with_latest_tag(config["gitlab"]["sm"]["project_id"], reghelper, "sm_fw_sha.is_latest_tag", LOGGER, api_tokens.get("gitlab", ""), sm_fw_hash, "gitlab")
    compare_git_version_with_latest_tag(config["github"]["cm"]["repo_name"], reghelper, "cm_fw.is_latest_tag", LOGGER, api_tokens.get("github", ""), cm_fw_hash, "github")
    compare_git_version_with_latest_tag(config["gitlab"]["ipmc"]["project_id"], reghelper, "ipmc_fw.is_latest_tag", LOGGER, api_tokens.get("gitlab", ""), ipmc_fw_hash, "gitlab")
    compare_git_version_with_latest_tag(config["gitlab"]["zynq"]["project_id"], reghelper, "zynq_fs.is_latest_tag", LOGGER, api_tokens.get("gitlab", ""), zynq_fs_hash, "gitlab")

def main():
    """
    Main entrypoint for this application.
    """
    # Check if the YAML config file is there
    if not os.path.exists(CONFIG_FILE_PATH):
        LOGGER.error(f'Cannot find configuration file: {CONFIG_FILE_PATH}, exiting.')
        return
    
    # Parse command line arguments
    args = parse_cli()

    LOGGER.info('')
    LOGGER.info('ApolloSM inventory monitor: Execution starting.')
    LOGGER.info('')

    # Load the configuration
    with open(CONFIG_FILE_PATH, 'r') as f:
        config = yaml.safe_load(f)

    try:
        sm_instance = ApolloSM([config['address_table']])

    except RuntimeError as e:
        LOGGER.info(f"Failed to create an ApolloSM from: {config['address_table']}")
        LOGGER.info(str(e))
        return

    LOGGER.info(f"Successfuly created an ApolloSM from: {config['address_table']}")

    if args.nodaemon:
        LOGGER.info("Not running as a daemon")
        func_daemon(config, sm_instance)

    else:
        # If we are running as daemon, make sure that the file handler
        # is passed in to the DaemonContext as a stream to preserve.
        # Otherwise, the stream gets blocked and no logging is done.
        with daemon.DaemonContext(
            working_directory="/tmp/", 
            pidfile=pidfile.PIDFile(PIDFILE_PATH),
            files_preserve=[file_handler.stream.fileno()]
            ) as context:
            func_daemon(config, sm_instance)


if __name__ == '__main__':
    main()
