# 
# Schema class defining an Apollo board (SM+CM).
# 

class ModuleBase():
    """
    Base class for Apollo Service Module and Command Module.
    """
    def __init__(self, identifier: str):
        # FW versions as read from SnipeIT DB and Zynq registers.
        self.fw_versions = {
            "fromDB"   : 0,
            "fromZynq" : 0,
        }
        self.fw_extra_versions = { #Specifically for SM_INFO.GLOBAL_SHA and GLOBAL_VER
            "fromDB"   : 0,
            "fromZynq" : 0,
        }
        self.uniqueID = {
            "fromDB"   : 0,
            "fromZynq" : 0,
        }
        self.fs_versions = {
            "fromDB"   : 0,
            "fromETC"  : 0,
        }

        # Identifier string (e.g. "ApolloSM" or "ApolloCM")
        self.identifier = identifier

    def set_firmware_version(self, type, hash):
        assert type in self.fw_versions, f"FW type must be either 'fromDB' or 'fromZynq'"
        self.fw_versions[type] = hash
    def set_firmware_extra_version(self, type, hash):
        assert type in self.fw_extra_versions, f"FW type must be either 'fromDB' or 'fromZynq'"
        self.fw_extra_versions[type] = hash
    def set_filesystem_version(self, type, hash):
        assert type in self.fs_versions, f"FW type must be either 'fromDB' or 'fromETC'"
        self.fw_versions[type] = hash
    def set_uniqueID(self, type, id):
        assert type in self.uniqueID, f"unique ID type must be either 'fromDB' or 'fromZynq'"
        self.uniqueID[type] = id

    def list_firmware_versions(self, logger):
        logger.info(f"{self.identifier} FW versions:")
        for key, version in self.fw_versions.items():
            logger.info(f"{key}: {version}")

    def fw_versions_match(self):
        """Returns True if the FW versions from DB and Zynq match."""
        return self.fw_versions["fromDB"] == self.fw_versions["fromZynq"]
    def fw_extra_versions_match(self):
        """Returns True if the FW extra versions from DB and Zynq match."""
        return self.fw_extra_versions["fromDB"] == self.fw_extra_versions["fromZynq"]
    def fs_versions_match(self):
        """Returns True if the FS versions from DB and ETC match."""
        return self.fw_versions["fromDB"] == self.fw_versions["fromETC"]
    def uniqueID_match(self):
        """Returns True if the unique ID versions from DB and Zynq match."""
        return self.uniqueID["fromDB"] == self.uniqueID["fromZynq"]

class ServiceModule(ModuleBase):
    """
    Class representing an Apollo Service Module.
    """
    def __init__(self, serial: int):
        super().__init__(identifier="ApolloSM")
        self.serial = serial

class IPMC(ModuleBase):
    """
    Class representing an Apollo IPMC.
    """
    def __init__(self):
        super().__init__(identifier="ApolloIPMC")

class CommandModule(ModuleBase):
    """
    Class representing an Apollo Command Module.
    """
    def __init__(self):
        super().__init__(identifier="ApolloCM")


class Zynq(ModuleBase):
    """
    Class representing an Apollo Zynq.
    """
    def __init__(self):
        super().__init__(identifier="ApolloZynq")


class Apollo:
    """
    Class representing an Apollo board with a Service and a Command Module.
    """
    def __init__(self, sm_serial: int):
        # Initialize the Service and Command Modules for this Apollo
        self.service_module = ServiceModule(sm_serial)
        self.command_module = CommandModule()
        self.ipmc = IPMC()
        self.zynq = Zynq()

    def list_firmware_versions(self, logger):
        """Prints the FW versions assigned to this ApolloSM instance."""
        self.service_module.list_firmware_versions(logger)
        self.command_module.list_firmware_versions(logger)
        self.ipmc.list_firmware_versions(logger)

    def set_ipmc_firmware_version(self, type, hash):
        """
        Set the FW version for the IPMC.
        
        type can be either "fromDB" or "fromZynq", representing
        from where the information was from.
        """
        self.ipmc.set_firmware_version(type, hash)

    def ipmc_fw_versions_match(self):
        """
        Returns True if FW versions from DB and Zynq match for IPMC. 
        """
        return self.ipmc.fw_versions_match()
    
    def set_ipmc_uniqueID(self, type, id):
        """
        Set the uniqueID for the IPMC.
        
        type can be either "fromDB" or "fromZynq", representing
        from where the information was from.
        """
        self.ipmc.set_uniqueID(type, id)

    def ipmc_uniqueID_match(self):
        """
        Returns True if unique ID from DB and Zynq match for IPMC. 
        """
        return self.ipmc.uniqueID_match()

    def set_zynq_filesystem_version(self, type, hash):
        """
        Set the FS version for the Zynq.
        
        type can be either "fromDB" or "fromETC", representing
        from where the information was from.
        """
        self.zynq.set_filesystem_version(type, hash)

    def zynq_filesystem_versions_match(self):
        """
        Returns True if Filesystem versions from DB and /etc/fs_version
        match for the Zynq.
        """
        return self.zynq.fs_versions_match()

    def set_zynq_uniqueID(self, type, id):
        """
        Set the uniqueID for the Zynq.

        type can be either "fromDB" or "fromZynq", representing
        from where the information was from.
        """
        self.zynq.set_uniqueID(type, id)

    def zynq_uniqueID_match(self):
        """
        Returns True if unique ID from DB and Zynq match for Zynq.
        """
        return self.zynq.uniqueID_match()

    def set_sm_firmware_version(self, type, hash):
        """
        Set the FW version for the SM.
        
        type can be either "fromDB" or "fromZynq", representing
        from where the information was from.
        """
        self.service_module.set_firmware_version(type, hash)

    def set_sm_firmware_extra_version(self, type, hash):
        """
        Set the FW version (extra) for the SM.

        type can be either "fromDB" or "fromZynq", representing
        from where the information was from.
        """
        self.service_module.set_firmware_extra_version(type, hash)

    def set_cm_firmware_version(self, type, hash):
        """
        Set the FW version for the CM.
        
        type can be either "fromDB" or "fromZynq", representing
        from where the information was from.
        """
        self.command_module.set_firmware_version(type, hash)

    def sm_fw_versions_match(self):
        """
        Returns True if FW versions from DB and Zynq match for SM. 
        """
        return self.service_module.fw_versions_match()

    def sm_fw_extra_versions_match(self):
        """
        Returns True if FW versions (extra) from DB and Zynq match for SM.
        """
        return self.service_module.fw_extra_versions_match()

    def cm_fw_versions_match(self):
        """
        Returns True if FW versions from DB and Zynq match for CM.
        """
        return self.command_module.fw_versions_match()
