from helpers.SMHelper import read_register, write_to_register
from ApolloSM import ApolloSM

class RegisterHelper():
    """
    Helper class to do register reads and writes using ApolloSM.
    """
    def __init__(self, registers: dict, sm_instance: ApolloSM, logger) -> None:
        self.registers = registers
        self.sm_instance = sm_instance
        self.logger = logger
    
    def _parse_reg_name(self, reg_name: str):
        tokens = reg_name.split(".")
        if len(tokens) != 2:
            self.logger.error(f"Invalid register name: {reg_name}")
            return
        
        return tokens

    def read(self, reg_name: str) -> int:
        """
        Read and return the value in a given register.
        """
        # Parse reg_name
        tokens = self._parse_reg_name(reg_name)
        if not tokens:
            return
        
        # Obtain the name of the register to read
        register = self.registers["read"][tokens[0]][tokens[1]]
        
        return read_register(register, self.sm_instance)


    def write(self, reg_name: str, value: int) -> None:
        """
        Write value to a register given by reg_name.
        """
        # Parse reg_name
        tokens = self._parse_reg_name(reg_name)
        if not tokens:
            return
        
        # Obtain the name of the register to write
        register = self.registers["write"][tokens[0]][tokens[1]]

        # Attempt a write
        self.logger.info(f"Writing {register} {hex(value)}")

        try:
            write_to_register(register, value, self.sm_instance)
        
        # Write failed
        except RuntimeError as e:
            self.logger.error(str(e))
            return
