# ApolloSM Inventory Monitor

Inventory monitoring software to keep track of Apollo Service Module (SM) and Command Module (CM) firmware versions. This software can be run as a daemon during boot, and it also supports a `--nodaemon` flag to run it as a non-daemon process.

## Setting Up The Code

This software is meant to be run on an Apollo SM, where all required Python3 packages are installed. These packages include: 

* requests
* PyYAML
* PyGithub

Once these are installed, below are the steps to set up and run this software.

### Clone The Repo

First, clone the repository with the `helpers` submodule:

```bash
git clone --recursive https://gitlab.com/apollo-lhc/software/apollosm-inventory-monitor.git
```

At any time, to update the `helpers` submodule, you can use:

```bash
git submodule update --recursive
```

### Get API Access Tokens

To run this code, you will need API access tokens for access to GitHub and SnipeIT APIs. These access tokens should be added to the YAML configuration file, with the top-level key name `api_tokens`. The formatting is expected to be: 

```yaml
api_tokens:
    snipeit: <API key>
    github:  <API key>
```

You can see an example in the `config.yaml` file. 

### Move The Config File

By default, this software will expect to see the YAML configuration file under the path `/etc/apollosm-inventory-monitor`. If the configuration file is not found under this path, the main process will exit, so please make sure that the configuration is in the right place. 

You can copy the `config.yaml` configuration to the designated path in the filesystem:

```bash
cp config.yaml /etc/apollosm-inventory-monitor
```

## Running The Code (No Daemon Mode)

If you want to run this code as a non-daemon process, just execute `apollosm-inventory-monitor.py` script as follows:

```
python3 apollosm-inventory-monitor.py --nodaemon
```

Note that the output will be logged to a log file, under `/var/log/apollosm-inventory-monitor.log`.

## Configuring The Application

A YAML config file is provided with the following settings:

* `registers`: List of register names to read for FW hash and SM serial number.
* `address_table`: Path to the XML address table on the SM.
* `poll_time`: Time between each polling of the application (in seconds).

More settings are also provided for the repo names for SM and CM FW, and also the Snipe IT URL to which the HTTP requests will be sent to.

These can be updated for custom use by the application user.

**Note:** As stated above, please make sure that this configuration is placed under `/etc/apollosm-inventory-monitor` in the filesystem.